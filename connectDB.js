const mysql = require('mysql');
const data = require('./settingsDB.json');

let pool = mysql.createPool({
    host: data.host,
    port: data.port,
    user: data.user,
    password: data.password,
    socketPath: data.socketPath,
    database: data.database
});

exports.query = function (sql, props) {
    return new Promise(function (resolve, reject) {
        pool.getConnection(function (err, connection) {
            connection.query(
                sql, props,
                function (err, res) {
                    if (err) reject(err);
                    else resolve(res);
                }
            );
            connection.release();
        });
    });
};
