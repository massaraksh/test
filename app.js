const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const query = require('./connectDB').query;
const error = require('./errorHandler').error;


app.listen(3000, function() {
  console.log("connect");
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function dataChecking(req, res, next) {
    const page = req.body.page;
    const limit = req.body.limit;
    console.log(isNaN(+page));
    if(isNaN(+page) || isNaN(+limit)) {
        error(res, 400, 'Incorrect data');
    } else {
        next();
    }
}
async function getData(req, res, next) {
  console.log(req.body);
  const page = req.body.page;
  const limit = req.body.limit;

  const sql = `select * from test limit ${(page-1)*limit}, ${limit}`;
  const sql1 = 'SHOW FIELDS FROM `test` FROM test';

  let resultBody = await query(sql);
  let body = [];

  for (let key in resultBody) {
      let data = [];
      for (let keyd in resultBody[key]) {
          data.push(resultBody[key][keyd]);
      }
      body.push(data);
  }


  let resultHead = await query(sql1);
  let head = [];

  for (let key in resultHead) {
      let data = [];
      for (let keyd in resultHead[key]) {
          data.push(resultHead[key][keyd]);
      }
      head.push(data);
  }

  let data = {
      'head': head,
      'body': body
  };
  res.json(data);
};

app.post('/getData', dataChecking, getData);

app.get('/', (req, res) => {
    res.header("Access-Control-Allow-Origin", "http://localhost");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.sendFile('index.html', { root: __dirname });
});

app.get('/js/jquery-3.2.1.min.js', (req, res) => {
    res.sendFile('js/jquery-3.2.1.min.js', { root: __dirname });
});

/*app.use(function(err, req, res, next) {             // [2]
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});*/