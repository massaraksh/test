/**
 * Created by massaraksh on 16.07.17.
 */
exports.error = function(res, status, errors, next) {
    const error = {
        status: status,
        errors: errors
    };

    res.status(status);
    res.json(error);
};